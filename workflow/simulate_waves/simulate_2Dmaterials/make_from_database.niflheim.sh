#!/bin/bash -le
#SBATCH --job-name=c2db_waves
#SBATCH --mail-type=START,END
#SBATCH --partition=xeon24
#SBATCH --output=slurm-%x-%j.out
#SBATCH --time=24:00:00
#SBATCH -N 1
#SBATCH -n 24

# Necessary modules
source ../../../../venv-xeon16/bin/activate

python make_from_database.py c2db -n -1 --repetitions=2 --debyewaller=0.003 --database=c2db-2021-06-24.db --maxsize=8

# Note: The script produces both the training and the test data.

