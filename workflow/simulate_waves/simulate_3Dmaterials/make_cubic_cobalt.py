## for importing modules from parent directories
import sys
sys.path.insert(0, '../../../../NeuralNetwork_HRTEM')
## import modules
import numpy as np
import matplotlib.pyplot as plt
import ase.io
import ase.build
import os
import time
import shutil
import argparse
import subprocess
from ase import Atoms
from ase.build import bulk
from ase.io import read
from ase.cluster.cubic import FaceCenteredCubic
from scipy.cluster.hierarchy import fcluster, linkage
from temnn.reproducible import make_reproducible
from abtem.potentials import Potential
from abtem.waves import PlaneWave
import multiprocessing

#########################################################
###### Function for monitoring memory usage #############
#########################################################
def print_memory(txt):
    procfile = open("/proc/self/status")
    vmsize = vmpeak = vmdata = vmrss = -1
    for line in procfile:
        words = line.split()
        if words[0] == "VmSize:":
            vmsize = int(words[1])
        elif words[0] == "VmPeak:":
            vmpeak = int(words[1])
        elif words[0] == "VmData:":
            vmdata = int(words[1])
        elif words[0] == "VmRSS:":
            vmrss = int(words[1])
    print("Memory : {} MB total ({} MB peak, {} MB data, {} MB rss)".format(
        (vmsize+512)/1024,(vmpeak+512)/1024,(vmdata+512)/1024,(vmrss+512)/1024),
        file=txt)
    procfile.close()

def driver2(args):
    driver(**args)
    
def driver(first_number, last_number, dir_name, N, sampling, tilt, seed):
    """Create nanoparticles, their exit waves and the ground truth for training.

    This function is creating a sequence of nanoparticles.  It is
    intended to be called in multiple processes in parallel, so the
    start and end number of the sequence is specified.

    first_number: int
        Sequence number of the first nanoparticle to be generated.
    last_number: int
        One more than the last nanoparticle to be generated.
    dir_name: string
        Name of the folder where the output is placed.
    N: int
        Size of the wavefunction array (will be N*N 128-bit complex numbers).
    sampling: float
        Resolution of the wavefunctions, in Angstrom per pixel.
    tilt: float
        Maximal off-axis tilt of nanoparticle, in degrees.  1.0 to 2.0 are good values.
    seed: np.random.SeedSequence or similar suitable seed
        Seed for the random number generator.
    """
    # Parameters
    L=sampling*N   # Cell size in Angstrom

    # Initialize random generator
    rng = np.random.default_rng(seed)
  
    # Read cubic CoSe2 computed structure from cif file
    ccobalt = read('CoSe2_mp-22309_computed.cif')

    # Create the models
    # The first mention of miller indices inclues the family of planes
    # The second mention allows for truncation of that specific layer
    for i in range(first_number, last_number):

        # randomly pick a z-thickness of the cell
        thick = rng.integers([L, L/2], endpoint=True)

        ## Build CoSe2 bulk
        rep = rng.integers(low=30, high=45, endpoint=True)
        ccobalt_bulk = ccobalt.repeat(int(rep))           # 100 direction along z-axis
        
        ccobalt_bulk.set_cell((L,L,thick))
        ccobalt_bulk.center()
        
        limit = 2.0                                       # Minimal amount of space along edges
        # Apply a random tilt        
        omega=rng.random()*360
        alpha=rng.random()*tilt
        ccobalt_bulk.rotate(v='z', a=omega, center='COU')
        ccobalt_bulk.rotate(v='y', a=alpha, center='COU')
        ccobalt_bulk.rotate(v='z', a=-omega, center='COU')
        
        # Random in-plane rotation
        zrot = rng.random()*360
        ccobalt_bulk.rotate(v='z', a=zrot, center='COU')

        # Cut out atoms outside the cell
        pos = ccobalt_bulk.positions
        keep = np.ones(len(atoms),bool)
        for j, p in enumerate(pos):
            if (p[0]>L-limit or p[1]>L-limit or p[2]>thick_choice-limit):
                keep[j] *= False
            elif (p[0]<0+limit or p[1]<0+limit or p[2]<0+limit):
                keep[j] *= False
        ccobalt_bulk = ccobalt_bulk[keep]

        ############################################
        ### Save positions for label generation ####
        ############################################
        ## For multiclass nanoparticle/substrate segmentation
        #positions = np.array([atoms.get_positions()[:,:2], slab.get_positions()[:,:2]],dtype='object')
        
        ## For single class cubic phase segmentation
        positions = ccobalt_bulk.get_positions()[:,:2]

        ## *** abTEM for TEM image ***
        # Determine number of points for each potential slice
        wave_xy = np.asarray(ccobalt_bulk.cell[0,0], ccobalt_bulk.cell[1,1])
        wave_pts = int(wave_xy/sampling)
        
        # Build potential for atoms
        # Number of slices is set by atoms.cell[2,2] with a default thickness of 0.5A
        potential = Potential(ccobalt_bulk,
                      sampling=sampling, 
                      gpts=wave_pts, 
                      parametrization='kirkland', 
                      projection='infinite')
        # Build wave
        wave = PlaneWave(
                energy=300e3     #acceleration voltage in eV
        )
        # Compute exit wave
        exit_wave = wave.multislice(potential,
                                 pbar=False)
        # Save coordinates of positions, sites, and heights
        np.savez('{0}/points/points_{1:04d}.npz'.format(dir_name,i),
                    positions=positions
                )
        
        # Save wavefunction in hdf5 format along (array, energy, extent)
        exit_wave.write('{0}/wave/wave_{1:04d}'.format(dir_name,i))
        #wave.save('{0}/wave/wave_{1:04d}.npz'.format(dir_name,i))
        ase.io.write('{0}/model/model_{1:04d}.cfg'.format(dir_name,i),ccobalt_bulk)
        print('   Constructed system {:2d} w/ {:5d} atoms'.format(i,
                                                     len(ccobalt_bulk),
                                                     flush = True))

        ### Log memory usage
        if os.path.exists("memory_usage.txt"):
            mode = 'a' # append if already exists
        else:
            mode = 'w' # make a new file if not
        with open("memory_usage.txt",mode) as text_file:
            print_memory(text_file)

def_resolution=1200
def_sampling=0.05

if __name__ == "__main__":
    # Use the standard argument parser - but also save the names of the positional parameters for
    # creating the reproducibility file.
    parser = argparse.ArgumentParser()
    positionalparams = ('folder', 'tilt', 'number')
    parser.add_argument("folder", help="The name of the folder (in ../simulation_data) where the output is placed.")
    parser.add_argument("tilt", help="Maximal off-axis tilt of the nanoparticle", type=float)
    parser.add_argument("number", type=int,
                        help="The desired number of training/testing examples (incl. any that are already done).")
    parser.add_argument("--sampling", type=float, default=def_sampling,
                        help="Sampling of wave function in Angstrom per pixel")
    parser.add_argument("--resolution", type=int, default=def_resolution,
                            help="Resolution of wave function in pixels.")
    parser.add_argument("-s", "--start", type=int, default=0,
                        help="Starting value (if some training data was already made).")
    parser.add_argument("-n", "--numproc", type=int, default=1,
                        help="Number of processes to use (CPU cores).  Use -1 for all available cores.")
    parser.add_argument('--train', dest='test', action='store_false',
                       help="Training data? (default)")
    parser.add_argument('--test', dest='test', action='store_true',
                       help="Test data?")
    parser.add_argument('--seed', type=int, default=None,
                        help="Seed for the random number generator")
    parser.set_defaults(test=False)
    args = parser.parse_args()

    if args.numproc > args.number:
        raise ValueError("Cannot run on more CPU cores than the number of training data.")

    if args.numproc < -1 or args.numproc == 0:
        raise ValueError("Number of CPU cores must be a positive integer, or -1 for all cores.")

    numproc = args.numproc
    if numproc == -1:
        if 'LSB_MAX_NUM_PROCESSORS' in os.environ:
            numproc = int(os.environ['LSB_MAX_NUM_PROCESSORS'])
        else:
            numproc = os.cpu_count()

    dir_name = os.path.join('../../', 'simulation_data', args.folder)
    seed = np.random.SeedSequence(args.seed)
    
    # Make sure working folders exist
    # Generate test set?
    if args.test==True:
        dir_name += '-test'
    if not os.path.exists(dir_name):
        print("Creating folder", dir_name)
        for subf in ['wave', 'model', 'points']:
            os.makedirs(os.path.join(dir_name, subf))
    
    print("Generating samples {} to {} in folder {} using {} process(es)\n".format(
        args.start, args.number, dir_name, numproc))
    print("np.random.SeedSequence entropy:", seed.entropy)

    # Keep a copy of this script for reference ...
    shutil.copy2(__file__, dir_name)
    # ... and write a shell script to reproduce this run.
    argdict = vars(args).copy()
    argdict['seed'] = seed.entropy
    argdict['numproc'] = numproc
    make_reproducible(os.path.join(dir_name, 'reproduce.sh'),
                        parser.prog,
                        argdict,
                        positionalparams)
    before = time.time()

    if numproc == 1:
        # Running on a single core.
        driver(first_number=args.start,
                last_number=args.number,
                dir_name=dir_name,
                N=args.resolution,
                sampling=args.sampling,
                tilt=args.tilt,
                seed=seed)
    else:
        # For abTEM compatability
        multiprocessing.set_start_method('spawn')
        data = []
        ndata = args.number - args.start
        seeds = seed.spawn(numproc)
        for i in range(numproc):
            data.append(dict(
                first_number=args.start + i * ndata // numproc,
                last_number = args.start + (i+1) * ndata // numproc,
                dir_name=dir_name,
                N=args.resolution,
                sampling=args.sampling,
                tilt=args.tilt,
                seed=seeds[i]
                ))
        with multiprocessing.Pool(numproc) as pool:
            pool.map(driver2, data)

    print("Time to simulate models: {:.2f} s.".format(time.time() - before))
