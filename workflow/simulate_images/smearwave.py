"""Smear out an exit wave by Fourier filtering it.

Works on the output files from the image simulation in images_labels,
and create a new set of files in a parallel folder called
smeared_XXXpm for data smeared by XXX pm (picometer).
"""

import numpy as np
from scipy import fft
import matplotlib.pyplot as plt
import json
import os
import argparse
import shutil

def gauss(x, y, sigma):
    return np.exp(- (x**2 + y**2) / (2 * sigma**2))

def process(indir, outdir, parameters, picometers):
    image_size = parameters['image_size']
    sampling = np.mean(parameters['sampling'])
    print("Image size:", image_size)
    print("Sampling:", sampling)
    sigma = picometers / 100 / sampling
    print("Sigma: {} pixels = {} Å".format(sigma, picometers/100))

    N = image_size[0]
    assert(N == image_size[1])
    fftsigma = N / (2 * np.pi * sigma)
    x = np.arange(N) - N//2
    fftgauss = gauss(x[:,np.newaxis], x[np.newaxis,:], fftsigma)
    fftgauss = fft.ifftshift(fftgauss)

    ndata = parameters['images_per_epoch']
    nepochs = parameters['image_epochs']
    print("Processing {} epochs of {} files".format(nepochs, ndata))
    for epoch in range(nepochs):
        print("Epoch {} of {}".format(epoch, nepochs))
        for i in range(ndata):
            filename = 'image_label_{:03d}_{:04d}.npz'.format(epoch, i)
            data = np.load(os.path.join(indir, filename))
            image = data['image']
            wave = data['label'][0]
            wave = wave[:,:,0] + 1j * wave[:,:,1]
            newwave = fft.ifft2(fft.fft2(wave) * fftgauss)
            smeared = np.empty_like(data['label'])
            smeared[0,:,:,0] = newwave.real
            smeared[0,:,:,1] = newwave.imag
            np.savez_compressed(os.path.join(outdir, filename),
                                    image=image, label=smeared)
            if i % 100 == 0:
                print("    {}".format(filename), flush=True)
                

if __name__  == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("data_dir", help="Path to directory with exit waves and images.")
    parser.add_argument("sigma", help="Smearing in picometer (integer).", type=float)
    args = parser.parse_args()

    if not os.path.isdir(args.data_dir):
        raise ValueError("Folder '{}' does not exit.".format(args.data_dir))

    indir = os.path.join(args.data_dir, "images_labels")
    pname = "parameters.json"
    parameterfile = os.path.join(args.data_dir, pname)
    if not os.path.isdir(indir):
        raise ValueError("Folder '{}' does not contain a subfolder 'images_labels'".format(args.data_dir))
    if not os.path.isfile(parameterfile):
        raise ValueError("No {} file in folder {}.".format(pname, args.data_dir))

    outdir = args.data_dir
    if args.data_dir.endswith('/'):
        outdir = outdir[:-1]
    data_dir_relative = os.path.join('..', os.path.basename(outdir))
    if outdir.endswith('-test'):
        outdir = outdir[:-5] + '_{:.0f}pm-test'.format(args.sigma)
    else:
        outdir = outdir + '_{:.0f}pm'.format(args.sigma)
    os.mkdir(outdir)
    shutil.copy2(__file__, outdir)
    for entry in os.scandir(args.data_dir):
        if entry.is_dir():
            os.symlink(os.path.join(data_dir_relative, entry.name),
                       os.path.join(outdir, entry.name))
        elif entry.is_file():
            shutil.copy2(entry.path, outdir)
    os.rename(os.path.join(outdir, 'images_labels'), os.path.join(outdir, 'images_labels.orig'))
    outname = "images_labels"
    outsubdir = os.path.join(outdir, outname)

    with open(parameterfile) as pf:
        parameters = json.load(pf)
    if not os.path.exists(outsubdir):
        print("Creating", outsubdir)
        os.mkdir(outsubdir)

    parameters['data_dir'] = outdir
    try:
        del parameters['debug_dir']
    except KeyError:
        pass
    with open(os.path.join(outdir, pname), 'wt') as pf:
        json.dump(parameters, pf, sort_keys=True, indent=4)
    process(indir, outsubdir, parameters, args.sigma)

    
