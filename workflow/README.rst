The folders are organised to follow the workflow.

simulate_Au
  Files for simulating clusters of Gold (Au) nanoparticles in the 110-orientation.

simulate_2Dmaterials
  Files for simulating 2D materials such as Graphene, Molybdenum-disulfide (MoS2), and hexagonal-Boron-Nitride (hBN).

simulate_images
  Files for precomputing HRTEM images for training and validation.

train_simulation
  Files for training. This includes separate scripts for the on the fly and precomputed workflow, as the images are
  either generated on the fly or loaded in from a directory, respectively. The suffix '-multi' feeds more than one
  image into the neural network. For example, three images at different focus settings are used as input to train
  the network to identify Molybdenum and Sulphur as separate classes in MoS2. 

validate_simulation
  File for validating. This includes seperate 'learningcurve' scripts for the on the fly and precomputed workflow. 
  A python notebook is included to plot the precision and recall values.
  
test_experimental
  Files for applying the neural network to experimental data.
