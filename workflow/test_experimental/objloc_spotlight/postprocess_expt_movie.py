#!/usr/bin/env python
# coding: utf-8
import sys
sys.path.insert(0, '../../../')

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import os
import hyperspy.api as hs
import skimage.io
#from stm.feature.peaks import find_local_peaks, refine_peaks
import argparse

def namegenerator(imgroot, predroot, filetype):
    """Generate input and output file names.

    Given the folder names, this generator makes matching input and
    output file paths, replicating the full folder tree of the input
    folder in the output folders.
    """
    for root, dirs, files in os.walk(imgroot):
        dirs.sort()
        files.sort()
        for f in files:
            fullname = os.path.join(root, f)
            f_noext, ext = os.path.splitext(fullname)
            if ext not in [filetype]:
                continue
            assert f_noext.startswith(imgroot)
            predname = predroot + f_noext[len(imgroot):] + '_inference.npz'
            yield fullname, predname

def get_image(f, crop=None):
    """Load an image, return data as a numpy array."""
    if f.endswith('.dm4'):   # Extend with .dm3 also?
        a = hs.load(f)
        image = a.data
        if crop is not None:
            image = image[crop[0]:crop[1],crop[2]:crop[3]]
    else:
        a = skimage.io.imread(f)
        image = skimage.color.rgb2gray(a)
        if crop is not None:
            image = image[crop[0]:crop[1],crop[2]:crop[3]]
    return image

def get_prediction(f):
    pred = np.load(f)['prediction']
    return pred

# In[ ]:

def makefigure(image, prediction, plotfile=None):
    prediction2 = prediction.copy()
    prediction2[prediction2 < 0.01] = np.nan
    
    fig, (ax_raw, ax_pred, ax_mask) = plt.subplots(1,3,figsize=(20,11))

    ax_raw.imshow(image,cmap='gray')
    ax_pred.imshow(prediction,cmap='gray')
    ax_mask.imshow(image,cmap='gray')
    ax_mask.imshow(prediction2,cmap='gnuplot_r',alpha=0.4)
    
    ax_raw.axis('off')
    ax_pred.axis('off')
    ax_mask.axis('off')
    plt.tight_layout()
    
    if plotfile:
        fig.savefig(plotfile, bbox_inches='tight')
        plt.close(fig)

# ## Run this section only for draft

# In[ ]:

#if False:
#    fi, fp = next(names)
#    print(fi)
#    image = get_image(fi,crop)
#    prediction = get_prediction(fp)
#    makefigure(image, prediction)

# Parse the command line
parser = argparse.ArgumentParser(description=__doc__)
parser.add_argument("datafolder",
                    help="The path and name of the folder where the experimental data is placed.")
parser.add_argument("analysisfolder",
                    help="The path and name of the folder where the analysed data is placed.")
parser.add_argument("--filetype",
                    help="The file extension of the experimental data (.dm4, .dm3, .png).",
                    default='.dm4')
#parser.add_argument("--sampling",
#                    help="Sampling in Angstrom/pixel (not critical, default: 0.1 Å/pixel)",
#                    default=0.1, type=float)
parser.add_argument("--crop", nargs='+',
        help="Crop the image (xmin, xmax, ymin, ymax)",
        default=None, type=int)
args = parser.parse_args()

imgroot = args.datafolder
dataroot = args.analysisfolder
# Remove trailing path separator (slash on unix, backslash on windows)
if not os.path.split(imgroot)[1]:
    imgroot = os.path.split(imgroot)[0]
assert not imgroot.endswith('/')
if not os.path.split(dataroot)[1]:
    dataroot = os.path.split(dataroot)[0]
assert not dataroot.endswith('/')
#sampling = args.sampling

predroot = dataroot + "_inference"
videoroot = dataroot + "_video"
scriptname = os.path.join(videoroot, "convertmovie.sh")
moviename = os.path.split(dataroot)[1] + ".m4v"

print("Reading images from folder {}".format(imgroot))
print("Output folder:", videoroot)
print("   Script file:", scriptname)
print("   Video file:", moviename)

# Produce the .png files for the movie
names = namegenerator(imgroot, predroot, filetype=args.filetype)

if not os.path.exists(videoroot):
    os.mkdir(videoroot)
    
# Incase cropping is required
crop = tuple(args.crop)

filenames = os.path.join(videoroot, 'files.txt')
shortfilenames = os.path.join(videoroot, 'files-short.txt')
with open(filenames, "wt") as f, open(shortfilenames, "wt") as sf:
    for i, (fi, fp) in enumerate(names):
        image = get_image(fi,crop=crop)
        print(fi)
        prediction = get_prediction(fp)
        pngfile = 'a{:04d}.png'.format(i)
        plotfile = os.path.join(videoroot, pngfile)
        makefigure(image, prediction, plotfile)
        f.write(plotfile + '\n')
        sf.write(pngfile + '\n')

# The images are converted to a movie using the `convert` utility from
# ImageMagick.  Unfortunately, it produces a video that PowerPoint
# cannot play, so it has to be converted again by a video conversion
# utility (I use Wondershare for Mac).
cmd = 'convert -delay 5 -quality 95 @{} {}'.format(os.path.split(shortfilenames)[1], moviename)
with open(scriptname, "wt") as script:
    print("#!/bin/bash", file=script)
    print(cmd, file=script)
# We skip running the actual video generation, as some installations do
# not have imagemagick and ffmpeg installed.
