#!/bin/bash

#!/bin/bash

if [[ -z "$CONDA_DEFAULT_ENV" ]]; then
    echo "Please activate the right Anaconda envronment before running this script." 1>&2
    exit
fi

if [[ -z "$CUDA_VISIBLE_DEVICES" ]]; then
    echo "Please select the desired GPU by setting CUDA_VISIBLE_DEVICES." 1>&2
    exit
fi

RESVLOCK=/tmp/lock-$DATASET-$$
python ../../reserve.py $RESVLOCK &
RESVPID=$!
sleep 10
echo "Locked GPU $CUDA_VISIBLE_DEVICES with lockfile $RESVLOCK and PID $RESVPID"

#DATASET=WilliamBang/20201126_ETEM_MEMS6
#DATASET=WilliamBang/20201201_ETEM_MEMS6/ROI1
#DATASET=WilliamBang/20210316_ETEM_MEMS8/AuCeO2_defoc9
#DATASET=au_spharm_19_tilt1_640p
DATASET=LiuPei/CoSe2-3-export

EXPSUBFOLDER=$DATASET
EXPFOLDER=/scratch/$USER/experimental_data/$EXPSUBFOLDER

mkdir -p $EXPFOLDER
rsync -av $RSYNCOPT hrid.fysik.dtu.dk:/u/raid/mhlla/$EXPSUBFOLDER/. $EXPFOLDER/.
#rsync -av $RSYNCOPT themis:/scratch/$USER/$SIMULSUBFOLDER/. $SIMULFOLDER/.

# Unlock
touch $RESVLOCK
wait $RESVPID
