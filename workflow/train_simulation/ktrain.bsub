#!/bin/bash
### General options
### -- specify queue --   NOTE: TitanX is significantly faster than K80
##BSUB -q gpuk80
#BSUB -q gpuv100
##BSUB -q gputitanxpascal
#BSUB -gpu "num=1:mode=exclusive_process"
### -- set the job Name --
#BSUB -J training_pre_StepPeak
### -- ask for number of cores (default: 1) --
#BSUB -n 20
#BSUB -R "span[hosts=1]"
### -- set walltime limit: hh:mm --  maximum 24 hours for GPU-queues right now
#BSUB -W 24:00
# request 5GB of memory
#BSUB -R "rusage[mem=5GB]"
### -- send notification at start --
##BSUB -B
### -- send notification at completion--
##BSUB -N
### -- Specify the output and error file. %J is the job-id --
### -- -o and -e mean append, -oo and -eo mean overwrite --
#BSUB -o training_pre_StepPeak-%J.out
###BSUB -e training-%J.err
# -- end of LSF options --

### Necessary modules ###
module load python3/3.7.6
module load cudnn/v7.6.5.32-prod-cuda-10.1

### Activate Python Virtual Environment with Tensorflow2 ###
source ~/.virtualenvs/project_tensorflow2/bin/activate

### On the fly training ###
## Unet architecture ##
# Gold nanoparticles #
#python ktrain_onthefly.py Unet ../simulation_data/Au_cluster-110-2deg5-big/ Au_cluster-110-2deg5-big

# MoS2 regular data set #
#python ktrain_onthefly.py UnetRP ../simulation_data/MoS2_regular_StepPeak/ MoS2_regular_StepPeak
#python ktrain_onthefly.py UnetRP ../simulation_data/MoS2_regular/ ~/$work3/UnetRP_onthefly_trained_data/MoS2_regular # hack to output data to work3 directory

# MoS2 supported data set #
#python ktrain_onthefly.py UnetRP ../simulation_data/MoS2_supported_StepPeak/ MoS2_supported_StepPeak
#python ktrain_onthefly.py UnetRP ../simulation_data/MoS2_supported/ ~/$work3/UnetRP_onthefly_trained_data/MoS2_supported # hack to output data to work3 directory


## MSDnet architecture  ##
# Gold nanoparticles #
#python ktrain_onthefly.py MSDnet ../simulation_data/Au_cluster-110-2deg5-big/ Au_cluster-110-2deg5-big

# MoS2 regular data set #
#python ktrain_onthefly.py MSDnetRP ../simulation_data/MoS2_regular_StepPeak/ MoS2_regular_StepPeak
#python ktrain_onthefly.py MSDnetRP ../simulation_data/MoS2_regular/ ~/$work3/MSDnetRP_onthefly_trained_data/MoS2_regular # hack to output data to work3 directory

# MoS2 supported data set #
#python ktrain_onthefly.py MSDnetRP ../simulation_data/MoS2_supported_StepPeak/ MoS2_supported_StepPeak
#python ktrain_onthefly.py MSDnetRP ../simulation_data/MoS2_supported/ ~/$work3/MSDnetRP_onthefly_trained_data/MoS2_supported # hack to output data to work3 directory

### Precomputed training ###
## Unet architecture ##
# Gold nanoparticles #
#python ktrain_precomputed.py Unet ../simulation_data/Au_cluster-110-2deg5-big/ Au_cluster-110-2deg5-big

# MoS2 regular data set #
python ktrain_precomputed.py UnetRP ../simulation_data/MoS2_regular_StepPeak/ MoS2_regular_StepPeak
#python ktrain_precomputed.py Unet ../simulation_data/MoS2_regular/ ~/$work3/Unet_precomputed_trained_data/MoS2_regular # hack to output data to work3 directory

# MoS2 supported data set #
python ktrain_precomputed.py UnetRP ../simulation_data/MoS2_supported_StepPeak/ MoS2_supported_StepPeak
#python ktrain_precomputed.py Unet ../simulation_data/MoS2_supported/ ~/$work3/Unet_precomputed_trained_data/MoS2_supported # hack to output data to work3 directory


## MSDnet architecture  ##
# Gold nanoparticles #
#python ktrain_precomputed.py MSDnet ../simulation_data/Au_cluster-110-2deg5-big/ Au_cluster-110-2deg5-big

# MoS2 regular data set #
python ktrain_precomputed.py MSDnetRP ../simulation_data/MoS2_regular_StepPeak/ MoS2_regular_StepPeak
#python ktrain_precomputed.py MSDnetRP ../simulation_data/MoS2_regular/ ~/$work3/MSDnetRP_precomputed_trained_data/MoS2_regular # hack to output data to work3 directory

# MoS2 supported data set #
python ktrain_precomputed.py MSDnetRP ../simulation_data/MoS2_supported_StepPeak/ MoS2_supported_StepPeak
#python ktrain_precomputed.py MSDnetRP ../simulation_data/MoS2_supported/ ~/$work3/MSDnetRP_precomputed_trained_data/MoS2_supported # hack to output data to work3 directory

