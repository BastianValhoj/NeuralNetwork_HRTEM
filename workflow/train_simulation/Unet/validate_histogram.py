"""validate_histogram.py - create a histogram of the network performance

Usage: python validate_histogram.py testdatafolder modelfolder [stepsubfolder]
"""

import sys
sys.path.insert(0, '../')

#import matplotlib
#matplotlib.use('AGG')

from train_imageepochs import datagenerator
import argparse
import os
import json
import numpy as np
from tensorflow import keras

def my_rmse(label, pred):
    assert (len(pred.shape) == 3 and pred.shape[2] == 2) or len(pred.shape) == 2  
    assert label.shape == pred.shape
    mse = np.mean((pred - label)**2)
    return np.sqrt(mse)

def my_rmss(label):
    mss = np.mean(label**2)
    return np.sqrt(mss)

def my_mae(label, pred):
    assert (len(pred.shape) == 3 and pred.shape[2] == 2) or len(pred.shape) == 2  
    assert label.shape == pred.shape
    return np.mean(np.abs(pred - label))

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "data",
        help="The pathname of the folder where the test data is placed."
        )
    parser.add_argument(
        "model",
        help="The pathname of the folder where the trained model is placed."
        )
    parser.add_argument(
        "modelstep",
        help="The subfolder with the model - default: 'model-0'.",
        nargs='?',
        default="model-0"
        )
    parser.add_argument(
        "-o", "--output",
        help="Name of output file",
        default=None
        )
    parser.add_argument(
        "--epoch",
        help="Use a specific data epoch (default: 0), for use with training data.",
        default=0,
        type=int
        )
    parser.add_argument(
        "--maximages",
        help="Limit the number of images used.",
        default=None,
        type=int
        )
    args = parser.parse_args()

    btch = 10
    with open(os.path.join(args.data, 'parameters.json'), "rt") as pfile:
        dataparameters = json.load(pfile)

    imgdim = tuple(dataparameters['image_size'])

    try:
        chan_in = dataparameters['multifocus'][0]
    except (KeyError, TypeError):
        chan_in = 1
    chan_out = dataparameters.get('num_classes', 1)
    images_per_epoch = dataparameters['images_per_epoch']
    if args.maximages and args.maximages < images_per_epoch:
        images_per_epoch = args.maximages
    
    #########################################################
    ################ load validation data ###################
    ### the input folders have been identified, including the
    ### validation dataset folder, here we load that in.
    ### keras will use this to validate the network after
    ### each training epoch
    #########################################################
    vinput_data_dir = args.data + '/images_labels'
    vdata_paths = \
    [
        os.path.join(vinput_data_dir, f'image_label_{args.epoch:03d}_{i:04d}.npz')
        for i in range(images_per_epoch)
    ]
    vinput_params_dir = args.data + '/tem_params'
    vparams_paths = \
    [
        os.path.join(vinput_params_dir, f'parameters_{args.epoch:03d}_{i:04d}')
        for i in range(images_per_epoch)
    ]
    print('validation data:', len(vdata_paths), ' samples')
    assert len(vdata_paths) == len(vparams_paths)
    
    val_gen = datagenerator(
            btch, imgdim, vdata_paths, vparams_paths, chan_in, chan_out,
            dataparameters['normalizedistance'], augment=False
    )

    print("\n\nLoading model", flush=True)
    modelname = os.path.join(args.model, args.modelstep)
    model = keras.models.load_model(modelname)
    
    print("\n\nMaking predictions", flush=True)
    predictions = model.predict(val_gen, verbose=1)

    print("Extracting labels ...", flush=True)
    labels = np.concatenate([x[1] for x in val_gen])
    print(len(labels))
    print(labels.shape, labels.dtype)
    print("... done.  Shape:", predictions.shape, predictions.dtype, labels.shape, labels.dtype)

    print(my_rmse(labels[0], predictions[0]), my_mae(labels[0], predictions[0]))
    print(my_rmse(labels[1], predictions[1]), my_mae(labels[1], predictions[1]))
    print(my_rmse(labels[0], predictions[1]), my_mae(labels[0], predictions[1]))

    print("Calculating", flush=True)
    rmse = np.array( [my_rmse(labels[i], predictions[i]) for i in range(len(labels))] )
    rmse_re = np.array( [my_rmse(labels[i,:,:,0], predictions[i,:,:,0]) for i in range(len(labels))] )
    rmse_im = np.array( [my_rmse(labels[i,:,:,1], predictions[i,:,:,1]) for i in range(len(labels))] )

    rel_rmse = np.array( [my_rmse(labels[i], predictions[i])  / my_rmss(labels[i])
                              for i in range(len(labels))] )

    mae = np.array( [my_mae(labels[i], predictions[i]) for i in range(len(labels))] )
    mae_re = np.array( [my_mae(labels[i,:,:,0], predictions[i,:,:,0]) for i in range(len(labels))] )
    mae_im = np.array( [my_mae(labels[i,:,:,1], predictions[i,:,:,1]) for i in range(len(labels))] )

    
    scram = np.arange(len(labels))
    np.random.shuffle(scram)
    scram_rmse = np.array( [my_rmse(labels[i], predictions[scram[i]]) for i in range(len(labels))] )
    scram_rmse_re = np.array( [my_rmse(labels[i,:,:,0], predictions[scram[i],:,:,0]) for i in range(len(labels))] )
    scram_rmse_im = np.array( [my_rmse(labels[i,:,:,1], predictions[scram[i],:,:,1]) for i in range(len(labels))] )

    scram_mae = np.array( [my_mae(labels[i], predictions[scram[i]]) for i in range(len(labels))] )
    scram_mae_re = np.array( [my_mae(labels[i,:,:,0], predictions[scram[i],:,:,0]) for i in range(len(labels))] )
    scram_mae_im = np.array( [my_mae(labels[i,:,:,1], predictions[scram[i],:,:,1]) for i in range(len(labels))] )

    if args.output is None:
        output = os.path.join(args.model, f"histogram-epoch-{args.modelstep}.npz")
    else:
        output = os.path.join(args.model, args.output)
        
    np.savez_compressed(output,
                        rmse=rmse, mae=mae, rel_rmse=rel_rmse,
                        rmse_re=rmse_re, rmse_im=rmse_im,
                        mae_re=mae_re, mae_im=mae_im,
                        scram_rmse=scram_rmse, scram_mae=scram_mae,
                        scram_rmse_re=scram_rmse_re, scram_rmse_im=scram_rmse_im,
                        scram_mae_re=scram_mae_re, scram_mae_im=scram_mae_im
                        )
    print(f"Result saved to {output}")

    
if __name__ == "__main__":
    main()
    
