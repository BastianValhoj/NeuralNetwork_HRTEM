import sys
sys.path.insert(0, '../../')
import argparse
import os
from glob import glob
import json
import numpy as np
import matplotlib.pyplot as plt
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2' # hide tensorflow output 
import tensorflow as tf
import tensorflow.keras as keras
from temnn.analysis.analysis import find_examples, plot_example

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("nn", help="Neural Network for training.")
    parser.add_argument("fn", help="Folder Name for validation.")
    args = parser.parse_args()
    
    # Import the specified neural network
    if args.nn == 'Unet':
        from temnn.knet import Unet
        net = Unet
        print('Imported Unet architecture')
    elif args.nn == 'MSDnet':
        from temnn.knet import MSDnet
        net = MSDnet
        print('Imported MSDnet architecture')
    else:
        print('Please specify a correct Neural Network (Unet, or MSDnet)')
    # Neural Network data
    if args.fn[-5:] == '-test':
        d_nn = '../{}_precomputed_trained_data/{}/'.format(args.nn,
                                                    args.fn[:-5])
    else:
        d_nn = '../{}_precomputed_trained_data/{}/'.format(args.nn,
                                                    args.fn)
    # Find the latest CNN model
    d_nn = glob(os.path.join(d_nn,'model-*'))[-1] # latest model
    mod = keras.models.load_model(d_nn)
    print("Using CNN model in", d_nn)
    
    # Open Parameters file
    fn = '../simulation_data/' + args.fn
    vflsin = os.path.join(fn, 'images_labels')
    with open(os.path.join(fn, 'parameters.json')) as json_file:
        vpar = json.load(json_file)
    noimgs = vpar['images_per_epoch'] # !! assuming one image epoch !! 
    imgdim = tuple(vpar['image_size']) # spatial dimensions of input/output
    if vpar.get('multifocus', None):
        chan_in = vpar['multifocus'][0]
    else:
        chan_in = 1 # depth of input data
    chan_out = vpar['num_classes'] # number of predicted class labels

    print("Loading validation set", flush=True)
    vinp = np.empty((noimgs,imgdim[0],imgdim[1],chan_in),
                                            np.float32)
    vtar = np.empty((noimgs,imgdim[0],imgdim[1],chan_out),
                                            np.float32)
    for i in range(noimgs):
        d = np.load(os.path.join(vflsin,
                    'image_label_{:03d}_{:04d}.npz'.format(0, i)))
        vinp[i] = (d['image'])
        vtar[i] = (d['label'])
        if (i + 1) % 100 == 0:
            print("    loaded {} images.".format(i+1), flush=True)

    idx_b, idx_w, idx_t = find_examples(mod, vinp, vtar)
    fig_b = plot_example(mod, vinp, vtar, idx_b)
    fig_b.savefig("best_example.png")
    fig_w = plot_example(mod, vinp, vtar, idx_w)
    fig_w.savefig("worst_example.png")
    fig_t = plot_example(mod, vinp, vtar, idx_t)
    fig_t.savefig("typical_example.png")
