========================================================================== 
Convolutional Neural Networks for analyzing atomic-resolution HRTEM images
==========================================================================


This is a collection of Python modules, scripts and Jupyter notebooks
for using Convolutional Neural Networks to identify atoms, atomic
columns and possibly other atomic-scale structures in High Resolution
Transmission Electron Microscopy (HRTEM) images.

This code is based on Jacob Madsen's code at
https://github.com/jacobjma/atomic-resolution-tensorflow.  Major parts
of the code are Jacob's, but the neural network itself has been
reimplemented with `Keras <http://keras.io>`_ instead of TensorFlow,
since the latter requires a significant amount of machine learning
expertise (or at least did when Jacob wrote his code).  Keras provides
a high-level interface and sensible defaults for most parameters.

The neural network can be trained on simulated images and subsequently
used on "real" HRTEM images (and image sequences) - this is
particularly useful for analyzing large amounts of images and long
sequences.


Microscopy & Microanalysis 2018
===============================

This folder contains a snapshot of the project as presented at the `M&M 2018 conference
<https://www.microscopy.org/mandm/2018/>`_

It also contains Jupyter Notebooks creating many of the figures for
the slides.  

A PDF of my presentation is also available as `MandM18_Schiotz.pdf <MandM18_Schiotz.pdf>`_.  Unfortunately, the
videos and animations cannot play in the PDF version.


What can it do?
===============

We have trained networks on Gold nanoparticles and used the network to
track surface diffusion of gold nanoparticles on an oxide support
under the influence of small amounts of gasses.

We have also trained a network on graphene-like sheets, and used the
network to find atoms in defected sheets of graphene, and thus
calculate the strain field in the graphene.

The methods are described in [JAMAD1]_



Installation
============

Installation instructions are in the file `INSTALL <INSTALL.rst>`_

Folders and Files
=================

Folders
-------

temnn
  Folders containing the software

  temnn/knet
    The Keras implementation of the Neural Network

  temnn/net
    Jacob Madsen's original code.  Contains both the raw TensorFlow
    implementation of the code (which is not used but kept for
    reference as it is the basis for [JAMAD1]_), and the files for
    handling simulated TEM dataset.

    dataset.py
      Defines objects for handling simulated TEM dataset and for
      operations on it.


simulation
  Scripts for generating training and test sets.  Python files
  starting with ``make_`` are scripts generating training and test
  sets.  Other Python files contain code used by these scripts.  The
  two Jupyter Notebooks are prototypes of the main Python scripts.


Jupyter Notebooks for the presentation:
---------------------------------------

Make Movie Intro.ipynb  (Slide 3)
  Illustrates how to make a movie from an image sequence that has
  already been analyzed with the CNN (analysis of sequences requires a
  GPU and cannot be done on my laptop).  The movie was used for the
  third slide at my M&M 2018 presentation.  This notebook requires an
  input movie not available as part of the project.

Microscope parameters.ipynb (Slide 4)
  Simulated HRTEM images illustrating how changing the microscope
  parameters changes the image.

Presentation graphs.ipynb (Slides 5 and 6)
  Illustrate activation functions and difference between fully
  connected and convolutional neural networks.

Featuremaps.ipynb (Slide 7)
  Stick man feature detection.

TrainingExamples.ipynb (Slide 9)
  Creates ten random training examples, four were chosen for slide 9.
  This notebook is saved without its output as the file size was
  otherwise excessive.  To run it, you also need to have povray
  installed so it can run from the command line (for example installed
  with Homebrew).

Make Movie Result.ipynb (Slide 10)
  Makes movie for result slide.  This notebook requires an
  input movie not available as part of the project.

Make Movie Result Peismovie.ipynb (Slide 11)
  As above.

CutImage.ipynb (Slide 12)
  Removes panel (c) from figure 6 in [JAMAD1]_.

PlotResolution.ipynb (Slide 14)
  Makes plot of recall and precision as function of resolution (within
  and without training range).

Graphene contrast inversion.ipynb (Slide 14)
  Makes illustration showing that the CNN for graphene
  can handle both positive and negative contrast within the same image
  (but gets confused right where the contrast inverts).

Other Jupyter notebooks
-----------------------

Jupyter notebooks are used for simple tasks that are most easily done
interactively, and for prototyping operations that are later run as
scripts.

ValidateVisual.ipynb
  Plot the raw output from the CNN together with the peaks found in
  both CNN output and ground truth.  Useful for figuring out what goes
  wrong when precision or recall drops.

Try expt with CNN.ipynb
  Illustrates loading an experimental image and running it through a
  pre-trained network.

Make Movie Draft.ipynb
  Notebook for experimenting with plotting parameters prior to making
  a movie.

Read Microscopy Files.ipynb
  How to read a .DM4 file in Python.

simulation/cluster_simulation.ipynb
  Drafting the generation of cluster training data.

simulation/graphene.ipynb
  Drafting the generation of graphene training data.


Python scripts
--------------

ktrain_np110_posdefocus.py
  Train a neural network.  Microscopy parameters and input folders are
  for Au nanoparticles in the <110> zone axis, resolution and
  parameters picked for the movie on slides 3 and 11.

ktrain_np110_posdefocus_2.py
  Similar, but for slide 10.  The resolution of the images in this
  sequence is much finer.  Cs allowed to change sign, as I do not know
  the actual parameters used when taking the data.  Using cross
  entropy loss instead of mean-square-error, as that performs
  marginally better.

ktrain_np110_defocus.py
  As above, but assuming negative defocus range.  Only used to test
  what happens when the sign of the defocus is "wrong" compared to the
  experimental conditions.

ktrain-graphene.py
  Trains for graphene, using both positive and negative defocus as in
  this case the structure makes it possible for the network to infer
  if atoms are black or white.

learningcurve*.py
  Calculates precision and recall for the training set and the
  validation set as a function of epoch.

validatedose*.py
  Precision and recall as a function of electron dose.

validatescale*.py
  Precision and recall as a function of resolution.  Goes widely
  outside the resolution range used for training in order to
  illustrate the importance of that parameter.

analyze_expt_movie.py
  Analyze an image sequence by running each image through the CNN.
  Output is two folders, one with the raw CNN output, the other with
  the positions of the peaks (much smaller amount of data).

evaluatepeaks.py
  Helper file to calculate precision and recall.  Should be moved
  elsewhere.



References
==========

.. [JAMAD1] Jacob Madsen, Pei Liu, Jens Kling, Jakob Birkedal Wagner,
	    Thomas Willum Hansen, Ole Winther and Jakob Schiøtz: *A
	    Deep Learning Approach to Identify Local Structures in
	    Atomic‐Resolution Transmission Electron Microscopy
	    Images*, Adv. Theory Simul. (in press, 2018).  DOI:
	    `10.1002/adts.201800037
	    <https://doi.org/10.1002/adts.201800037>`_.  Preprint
	    available at https://arxiv.org/abs/1802.03008
	    
