"""Calculate precision and recall on the testset."""

from glob import glob
import numpy as np
import keras
from keras.utils import multi_gpu_model
import tensorflow as tf
from temnn.knet import net
from temnn.imagesimul.makeimages import MakeImages
from temnn.loaddata import load, load_CNN
# Peak detection
from temnn.imagesimul.evaluatepeaks import precision_recall, evaluate_result
import sys
import os
from multiprocessing import Pool
import shutil
from natsort import natsorted
import json

# This script takes one argument: the name of the folder where the
# trained neural network is placed.
if len(sys.argv) >= 2:
    graph_dir = sys.argv[1]
else:
    print("\n\nUsage: {} foldername".format(sys.argv[0]), file=sys.stderr)
    sys.exit(-1)

graph_path = os.path.join(graph_dir, 'clusters-*.h5')
result = os.path.join(graph_dir, 'scalecurve.dat')
parameterfile = os.path.join(graph_dir, 'parameters.json')

with open(parameterfile, "rt") as pfile:
    parameters = json.load(pfile)

# Even if training generated debug images, we do not want to do it now.
parameters['debug'] = False

# The validation data is in a sister-folder to the training data
ddir = parameters['data_dir']
assert ddir.endswith('/')
parameters['validation_dir'] = ddir[:-1] + '-test/'

# We only need one GPU, and since we do not train we can have more images in a batch
num_gpus = 1
batch_size = 8 * num_gpus

# We read the number of images and the number of classes from the parameter file.
num_classes = parameters.get('num_classes', 1)
try:
    num_images = parameters['multifocus'][0]
except KeyError:
    num_images = 1

image_size = tuple(parameters['image_size'])
data_train = load(parameters['data_dir'])
n_train = data_train.num_examples
print("Number of training images:", n_train)
data_valid = load(parameters['validation_dir'])

n_valid = data_valid.num_examples
print("Number of validation images:", n_valid)

# Keep a copy of this script for reference
shutil.copy2(__file__, graph_dir)

# Find the latest CNN
print("Looking for CNNs in files matching", graph_path)
gr = list(natsorted(glob(graph_path)))[-1]
print("Using CNN parameters in", gr)
x, model = load_CNN(gr, net.graph, image_size, num_gpus,
                    image_features=num_images, num_classes=num_classes)

with open(result, "wt") as outfile:
    for step, sampling in enumerate(np.arange(0.05, 0.2, 0.01)):
        print("Evaluating sampling", sampling, flush=True)
        
        linedata = [sampling]
        parameters['sampling'] = [sampling, sampling]
        imagestream_train = MakeImages(data_train, parameters, seed=parameters['seed'])
        imagestream_valid = MakeImages(data_valid, parameters, seed=parameters['seed_validate'])
        maxcpu = imagestream_train.maxcpu
        for (n, imagestream) in ((n_train, imagestream_train), (n_valid, imagestream_valid)):
            #n = 25
            result = []

            print("Getting all images", flush=True)
            images, labels = imagestream.get_all_examples()
            print("Making preditions with CNN.", flush=True)
            predictions = model.predict(np.array(images), batch_size=batch_size)

            # Now we have an array with predicted images (predictions) and
            # one with expected images (labels).  We now need to calculate
            # precision and recall in parallel

            print("Processing predictions.", flush=True)
            with Pool(maxcpu) as pool:
                result = pool.starmap(evaluate_result,
                                      zip(predictions, labels, [sampling]*len(labels)))
            
            result = np.array(result)
            precision = result[:,0].mean()
            recall = result[:,1].mean()
            linedata.extend((precision, recall))
        line = "{:8.4f}  {:8.6f}  {:8.6f}  {:8.6f}  {:8.6f}".format(*tuple(linedata))
        print("*****", line, flush=True)
        print(line, file=outfile, flush=True)
