"""Helper functions for loading training data and neural nets."""
import keras
from temnn.data.dataset import DataEntry,DataSet
from glob import glob

def load_training(data_dir):
    "Load data folder."
    print("Data source:", data_dir)
    waves=sorted(glob(data_dir+"wave/wave_*.npz"))
    points=sorted(glob(data_dir+"points/points_*.npz"))
    entries = [DataEntry(wave=w, points=p) for w,p in zip(waves,points)]
    if not len(entries):
        raise ValueError("The dataset appears to be emtpy!")
    return DataSet(entries)

# For script compatibility
load = load_training

def load_CNN(graph_path, graph_func, size, num_gpus=1, image_features=1, num_classes=1):
    "Load the Keras neural net, and return a Model."
    if num_gpus == 1:
        x = keras.Input(shape=tuple(size)+(image_features,))
        model = graph_func(x, output_features=num_classes)
        model.load_weights(graph_path)
    else:
        with tf.device('/cpu:0'):
            x = keras.Input(shape=tuple(size)+(image_features,))
            model = net.graph(x, output_features=num_classes)
            model.load_weights(graph_path)
        model = multi_gpu_model(model, gpus=num_gpus)

    model.compile(optimizer='rmsprop', loss='binary_crossentropy')
    
    return (x, model)
