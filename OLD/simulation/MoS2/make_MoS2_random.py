import sys
sys.path.append('..')  # Ugly hack!  Allows to import from the parent folder.
import numpy as np
import lloyds
from poisson_disc import Grid
from labels import project_positions,create_label
from temnn.data.dataset import DataSet, DataEntry
from ase import Atoms,Atom
from pyqstem import PyQSTEM
from pyqstem.imaging import CTF
from pyqstem.util import atoms_plot
import matplotlib.pyplot as plt
import matplotlib.path as mplPath
from scipy.spatial import Voronoi,KDTree
from ase.io import write,read
from ase.build import graphene_nanoribbon
import scipy.spatial
from ase.visualize import view
import collections
from glob import glob
import os
import argparse
from multiprocessing import Pool

def lookup_nearest(x0, y0, x, y, z):
    xi = np.abs(x-x0).argmin()
    yi = np.abs(y-y0).argmin()
    return z[yi,xi]

def interpolate_smoothly(points, N):
    half = (len(points) + 1) // 2
    F = np.fft.fft(points)
    F = np.hstack((F[:half],[0]*(len(F)*N),F[half:]))
    return np.fft.fft(F)[::-1].real / len(points)


def blob(N=10,M=50):
    points=np.array([np.random.rand(N),np.random.rand(N)]).T
    points=points[scipy.spatial.ConvexHull(points).vertices]
    points=np.array([interpolate_smoothly(p, M) for p in zip(*points)]).T
    return points-np.mean(points,axis=0)

# Not used anywhere :/
def strain(positions,direction,cell,power=-3,amplitude=10**3,N=(64,64)):
    noise=spectral_noise.power_law_noise(N,power)
    x=np.linspace(0,cell[0],N[0])
    y=np.linspace(0,cell[1],N[1])

    positions[:,direction]+=amplitude*np.array([lookup_nearest(p[0], p[1], x, y, noise) for p in positions]).T
    
    return positions

def random_sheet(cell,r):
    
    grid = Grid(r, cell[0], cell[1])
    
    rand = (np.random.uniform(0, cell[0]), np.random.uniform(0, cell[1]))
    positions = grid.poisson(rand)
    
    positions=lloyds.repeat(positions,cell[:2])
    
    vor=Voronoi(positions)
    
    positions=vor.vertices
    
    positions=positions[positions[:,0]<cell[0]]
    positions=positions[positions[:,0]>0]
    positions=positions[positions[:,1]<cell[1]]
    positions=positions[positions[:,1]>0]
    
    positions=lloyds.relax(positions,cell[:2],num_iter=1,bc='periodic')
    positionsHoles = np.copy(positions) # Make a copy used for the holes
    
    # Make the holes
    num_holes=np.random.randint(0,3)
    for i in range(num_holes):
        size=(.4+.6*np.random.rand())*cell[0]
        hole=size*blob()+[np.random.uniform(0, cell[0]), np.random.uniform(0, cell[1])]
        contained = mplPath.Path(hole).contains_points(positionsHoles)
        positionsHoles = positionsHoles[np.where(contained==0)[0]]
    
    positions=positionsHoles
    positions=np.hstack((positions,np.array([[0]*len(positions)]).T))
    
    atoms=Atoms(['C']*len(positions),positions*1.2)
    atoms.set_cell(cell)
    atoms.set_positions(positions)
    atoms.set_pbc(1)
    atoms.wrap()
    atoms.center()
    
    atoms = ChangeAtoms(atoms) # Alchemy: Changes graphene into MoS2
            
     #Inserts the values from the array into the atom
    print(len(atoms), len(atoms)/(cell[0]*cell[1]), 3/8.78)
    
    return atoms

def load(data_dir):
    
    waves=glob(data_dir+"wave/wave_*.npz")
    labels=glob(data_dir+"label/label_*.npy")

    entries=[DataEntry(wave=w,label=l) for w,l in zip(waves,labels)]
    
    return DataSet(entries)

def ChangeAtoms(atoms):
    
    sites = atoms.get_positions()
    newtypes = np.zeros(len(atoms))
    
    stack = collections.deque() 
    neighborlist = KDTree(sites) 
    _, pos = neighborlist.query(atoms.get_cell().diagonal() / 2, 1) #Gets the length and index (iAtom) for the 4 nearest atom
    stack.append([pos, 42]) # Start by requesting atom 0 to be Mo

    while len(stack)>0:
        pos, AtomType = stack.pop() #Takes the first entrys position and type 
    
        if newtypes[pos] == 0 :
            # Change atom from C to Mo or S.
            newtypes[pos] = AtomType
            lAtom, iAtom = neighborlist.query(sites[pos],k=4)  # Three neighbors and itself
            # The first is the atoms itself, the second its closest neighbor
            cutoff = 1.5 * lAtom[1]

            for i in range(4):
                if newtypes[iAtom[i]] == 0:  # and lAtom[i] < cutoff:
                    if AtomType == 42:
                        stack.appendleft([iAtom[i], 16])
                    else:
                        assert(AtomType == 16)
                        stack.appendleft([iAtom[i], 42])

        # Check if done - restart if missing atoms elsewhere (in case of disconnected regions)
        if len(stack) == 0:
            if (0 in newtypes) == True:
                stack.appendleft([np.argmin(newtypes), 42])  # Restart algorithm
            
    atoms.set_atomic_numbers(newtypes) # Insert the new atomic numbers
    
    # Transform each S atom to a dimer displaced by dz above and below plane.
    dz = 1.56
    sites = atoms.get_positions()
    NewS = Atoms() # List of new sulphur atoms
    for i in range(len(sites)):
        if atoms.get_atomic_numbers()[i] == 16:
            sites[i][2] = sites[i][2]+dz
            NewS+=Atom('S',position=sites[i]+[0,0,-dz*2])
            
    atoms.set_positions(sites) # This translates the old S atoms
    atoms+=NewS #And add the new ones.
    
    return atoms


def driver(args):
    driver2(**args)

def driver2(start, end, folder):
    for i in range(start, end):
        make_training_set(i, folder)

def make_training_set(i, folder):
    N=1024
    sampling=0.05
    L=sampling*N
    cell=(L,L,8)
    label_size=(N,N)

    atoms=random_sheet(cell,2.45)
    
    qstem=PyQSTEM('TEM')
    
    image_size=(N,N)

    qstem.set_atoms(atoms)
    qstem.build_wave('plane',80,image_size)
    qstem.build_potential(int(atoms.get_cell()[2,2]*2))
    qstem.run()
    wave=qstem.get_wave()
    wave.array=wave.array.astype(np.complex64)

    # Get positions directly from the atoms.  Take into account that
    # the two sulphur atoms should count as one 2D position.  This is
    # done by skipping atoms with negative z.
    pos = atoms.get_positions()
    meanz = pos[:,2].mean()
    atno = atoms.get_atomic_numbers()
    positions = [(x, y) for x, y, z in pos if z > meanz - 0.1]
    cls = {42:0, 16:1}
    classes = [cls[z] for z, p in zip(atno, pos) if p[2] > meanz - 0.1]
    positions = np.array(positions, np.float32)
    classes = np.array(classes, np.int8)
    print(positions.shape, positions.dtype, classes.shape, classes.dtype)
    assert len(positions) == len(classes)
    assert classes.min() == 0 and classes.max() == 1
    
    #label=create_label(positions,label_size,6)
    
    #np.save('{0}/label/label_{1:04d}.npy'.format(folder,first_number+i),label)
    np.savez('{0}/points/points_{1:04d}.npz'.format(folder,i), sites=positions, classes=classes)
    write('{0}/model/model_{1:04d}.cfg'.format(folder,i),atoms)
    wave.save('{0}/wave/wave_{1:04d}.npz'.format(folder,i))
    
    print('iteration',i)
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("folder", help="The name of the folder (in ../data) where the output is placed.")
    parser.add_argument("number", type=int,
                        help="The desired number of training examples (incl. any that are already done).")
    parser.add_argument("-s", "--start", type=int, default=0,
                        help="Starting value (if some training data was already made).")
    parser.add_argument("-n", "--numproc", type=int, default=1,
                        help="Number of processes to use (CPU cores).  Use -1 for all available cores.")
    args = parser.parse_args()

    if args.numproc > args.number - args.start:
        raise ValueError("Cannot run on more CPU cores than the number of training data.")

    if args.numproc < -1 or args.numproc == 0:
        raise ValueError("Number of CPU cores must be a positive integer, or -1 for all cores.")

    numproc = args.numproc
    if numproc == -1:
        if 'LSB_MAX_NUM_PROCESSORS' in os.environ:
            numproc = int(os.environ['LSB_MAX_NUM_PROCESSORS'])
        else:
            numproc = os.cpu_count()

    dir_name = os.path.join('..', 'data', args.folder)

    print("Generating samples {} to {} in folder {} using {} process(es)".format(
        args.start, args.number, args.folder, numproc))

    # Make sure working folders exist
    if not os.path.exists(dir_name):
        print("Creating folder", dir_name)
        for subf in ['wave', 'model', 'points']:
            os.makedirs(os.path.join(dir_name, subf))

    if numproc == 1:
        # Running on a single core.
        for i in range(args.start, args.number):
            make_training_set(i, folder=dir_name)
    else:
        data = []
        ndata = args.number - args.start
        for i in range(numproc):
            data.append(dict(
                start=args.start + i * ndata // numproc,
                end = args.start + (i+1) * ndata // numproc,
                folder=dir_name
                ))
        with Pool(numproc) as pool:
            pool.map(driver, data)

