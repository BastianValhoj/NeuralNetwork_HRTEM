"""Create images for training and testing of the neural network."""
import os
import numpy as np
from collections import deque
from multiprocessing import Pool
from .imagesimulation import makeimage
import itertools
import json

# Silence stupid warnings in Python 3.8 on some installations
import warnings
from cryptography.utils import CryptographyDeprecationWarning
warnings.filterwarnings(action='ignore',category=CryptographyDeprecationWarning)

# Use multiprocessing to generate many sample datasets
class MakeImages:
    # The number of random numbers pregenerated per image is set and stored here.
    # It is chosen overly large, so addition of new algorithms are unlikely to
    # require changing this parameter.  In addition, it will be read an stored
    # in the parameter file, and possibly overridden from the parameter file,
    # to minimize the risk of spoiling reproducibility of old calculation if a new
    # algorithm requires increasing this number
    randomvectorlength = 100
    
    def __init__(self, data, parameters, label, seed, batch=None, maxcpu=None):
        
        self.data = data
        self.precomputed = []
        self.parameters = parameters.copy()
        self.n = 0
        if maxcpu:
            self.maxcpu = maxcpu
        else:
            # Check for the max number of processors we are allowed to use.
            # Works both with SLURM and LSM, gives all cores if no batch system.
            self.maxcpu = len(os.sched_getaffinity(0))
        if batch is None:
            self.batchsize = max(5 * self.maxcpu, 100)
        else:
            self.batchsize = batch
        print("Setting max number of CPUs to {} with a batch size of {}".format(
            self.maxcpu, self.batchsize), flush=True)
        # Use a local random number generator that can optionally be seeded.
        #self.seed = seed
        self.random = np.random.default_rng(seed)
        self.label = label
        if 'randomvectorlength' in parameters:
            self.randomvectorlength = parameters['randomvectorlength']

    def reset(self):
        print("Resetting:", self.data._index_in_epoch)
        self.data._index_in_epoch = 0
        if self.seed:
            self.random.seed(self.seed)

    def makebatch(self, batchsize):
        entries = self.data.next_batch(batchsize, shuffle=None)
        sequence = np.arange(self.n, self.n + batchsize)
        self.n += batchsize
        # Make overly many random numbers per image, as 
        rndnums = self.random.uniform(0.0, 1.0, size=(batchsize, self.randomvectorlength))
        params = [self.parameters,] * batchsize
        label = [self.label,] * batchsize
        if self.maxcpu == 1:
            # Don't use multiprocessing to ease debugging.
            return map(makeimage, entries, params, sequence, rndnums, label)
        with Pool(self.maxcpu, maxtasksperchild=25) as pool:
            return pool.starmap(makeimage, zip(entries, params, sequence, rndnums, label))

    def precompute(self):
        self.precomputed = deque(self.makebatch(self.batchsize))

    def next_example(self):
        if not self.precomputed:
            self.precompute()
        return self.precomputed.popleft()

    def get_all_examples(self):
        "Get an example from each data point."
        images = []
        labels = []
        for img, lbl in self.makebatch(self.data.num_examples):
            images.append(img)
            labels.append(lbl)
        return np.concatenate(images), np.concatenate(labels)

    def write_all_examples(self, imglblfile, temparamfile):
        """Make and save an image for each system.

        This creates a single image epoch (in parallel on multiple cores)

        Returns: Number of images created.
        """
        batchsize = self.data.num_examples
        entries = self.data.next_batch(batchsize, shuffle=None)
        # Number in this sequence (for file names)
        sequence = np.arange(batchsize)
        # Number in global sequence (for debugging output)
        globalsequence = np.arange(self.n, self.n + batchsize) 
        self.n += batchsize
        # Make overly many random numbers per image, as 
        rndnums = self.random.uniform(0.0, 1.0, size=(batchsize, self.randomvectorlength))
        params = itertools.repeat(self.parameters, batchsize)
        label = itertools.repeat(self.label, batchsize)
        imglbl = itertools.repeat(imglblfile, batchsize)
        temparam = itertools.repeat(temparamfile, batchsize)
        if self.maxcpu == 1:
            # Don't use multiprocessing to ease debugging.
            retval = map(makeimage_save, entries, params, sequence, globalsequence, rndnums, label, imglbl, temparam)
        with Pool(self.maxcpu, maxtasksperchild=25) as pool:
            retval = pool.starmap(makeimage_save, zip(entries, params, sequence, globalsequence, rndnums, label, imglbl, temparam))
        return len(retval) # Number of images processed

def makeimage_save(entry, params, imgnum, gl_imgnum, rndnums, labeltype, imfile, paramfile):
    image, label, tem_params = makeimage(entry, params, gl_imgnum, rndnums, labeltype)
    filename = imfile.format(imgnum)
    np.savez_compressed(filename,
                        image=np.asarray(image).astype('float32'),
                        label=np.asarray(label).astype('float32'))
    # Also store the parameters in a machine_readable file
    with open(paramfile.format(imgnum), "wt") as pfile:
        json.dump(tem_params, pfile, sort_keys=True, indent=4)
    if imgnum % 100 == 0:
        print('Image and label saved as ', filename, flush=True)
    return imgnum
