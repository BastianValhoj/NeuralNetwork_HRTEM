=========================================
HOW TO RUN ON THE DTU COMPUTE GPU CLUSTER
=========================================

Information about the cluster is available at https://itswiki.compute.dtu.dk/index.php/GPU_Cluster

Installing the required software
================================

Add the following line to your ``.bashrc`` file::

  module load cuda10.2-trt7

and either log out and in again, or just execute the same command in your shell.

The DTU compute system uses miniconda to install software.  This also
provides a way to use different installed software for different
projects through the use of environments.  Create an environment (let's call it ``hrtem``), and
install as much software as you can from the Anaconda infrastructure,
and the rest with pip::

  conda create -n hrtem python=3.7
  conda activate hrtem
  conda install scipy matplotlib cython fftw scikit-image natsort tqdm
  conda install tensorflow-gpu
  conda install notebook
  pip install pyqstem

Notes:

#. Tensorflow currently does not work with Python 3.8, so we use version 3.7.
   
#. Tensorflow must be installed as ``tensorflow-gpu`` to include the
   GPU support, this differs from how you install it with pip.

#. PyQstem is best installed with pip, after you have installed most
   prerequisites.  ASE installed as a dependency.

#. You should not use the ``--user`` argument to pip when installing
   in an Anaconda environment.

Now you can download the software from GitLab::

  git clone https://gitlab.com/schiotz/NeuralNetwork_HRTEM

Running the code
================

**IMPORTANT:** Please do **not** run the scripts in ``simulate-Au`` and similar on the GPU cluster.  You will block all CPUs and slow down the GPU jobs of other users.  Instead, run them on the DTU HPC system.

1. Select a machine from the cluster.  Check if there is a free GPU
   using ``gpustat``.  Repeat until you find a machine with free
   space.

2. Copy the training data to ``/scratch`` using rsync.  You may need
   to create a folder on ``/scratch`` first.  Note that this file
   system is local for each machine, it is not shared::

      mkdir /scratch/$USER
      rsync -av transfer.gbar.dtu.dk:NeuralNetworkHTREM/simulation_data /scratch/$USER
      ln -s /scratch/$USER/simulation_data   # Only needed once!

3. Check that there is still a free GPU.  Note its number (say it is
   number 2).  Run the training script like this::

      CUDA_VISIBLE_DEVICES=2 python ktrain.py Unet ../simulation_data/Au_cluster-110-2deg5-big/ Au_cluster-110-2deg5-big

   Note that the "2" does not mean use two devices, it means run on
   device with ID number 2.  If you want to run on multiple devices,
   separate their IDs with commas.


